<?php
/**
 * @file
 * Stock Exchange Connector block theme file.
 */

drupal_add_js('https://code.jquery.com/jquery-1.11.3.min.js', 'external');
drupal_add_js(drupal_get_path('module', 'stock_exchange_connector') . '/js/jquery.jstockticker-1.1.1.js');
drupal_add_js(drupal_get_path('module', 'stock_exchange_connector') . '/js/utilities.js');
drupal_add_css(drupal_get_path('module', 'stock_exchange_connector') . '/css/utilities.css');

$xmls = simplexml_load_file(drupal_get_path('module', 'stock_exchange_connector') .'/files/stock_data.xml');
?>

<div id="stock-exchange-block">
    <div id="ticker-wrapper">
        <div id="ticker" class="stockTicker" style="width: 10000000px">
            <span class="quote">Stock Rates: </span>
            <?php foreach($xmls->trades->trade as $item){  ?>
                <span class="up"><span class="quote"><?php print $item->symbol; ?></span> <span class="down"><?php print $item->price.' <span class="eq">'.$item->percentageChange.'%</span>'; ?></span></span>
           <?php } ?>
        </div>
    </div>
</div>
