/**
 * @file
 * Javascript file for the Gmail Connector.
 *
 * Javascript file for the Google Connector.
 */

/* global Drupal Pace gapi window to:true body:true subject:true btoa jQuery alert setTimeout data_length:true */
/* eslint strict: [2, "never"]*/

(function ($) {
    Drupal.behaviors.gmail_connector = {
      attach: function (context, settings) {
          $("#ticker").jStockTicker({interval: 10});
          }
    };
  })(jQuery);
